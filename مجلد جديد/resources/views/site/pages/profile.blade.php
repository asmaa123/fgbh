@extends('site.layouts.master')
@section('title')
الصفحة الشخصية 
@endsection
@section('content')      
<!-- Content page Start -->
<div class="content-wrapper">
	<section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{asset('storage/uploads/users').'/'.Auth::guard('members')->user()->image}}" alt="User profile picture">
                  <h3 class="profile-username text-center">{{Auth::guard('members')->user()->guardian_name}}</h3>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">المعلومات الشخصية</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-briefcase margin-r-5"></i> الوظيفة</strong>

              <p class="text-muted">
              {{Auth::guard('members')->user()->job}}
              </p>

              <hr>

              <strong><i class="fa fa-globe margin-r-5"></i> الجنسية</strong>

              <p class="text-muted">{{Auth::guard('members')->user()->nationality}}</p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> العنوان</strong>

              <p>
              {{Auth::guard('members')->user()->address}}
              </p>

              <hr>

              <strong><i class="fa fa-mobile margin-r-5"></i> رقم الهاتف</strong>

              <p>{{Auth::guard('members')->user()->phone}}</p>

              <hr>

              <strong><i class="fa fa-envelope margin-r-5"></i> البريد الالكترونى</strong>

                <p class="text-muted">
                {{Auth::guard('members')->user()->email}}
                </p>

                <hr>
                <strong><i class="fa fa-users margin-r-5"></i>  الطلاب</strong>
                @foreach($students as $student)
                <p>
                {{$student->student_name}}
                </p>

                @endforeach
                
            </div>
            <!-- /.box-body -->
          </div>

            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#settings" data-toggle="tab" >الاعدادات</a></li>
                  <li><a href="#img" data-toggle="tab">تغير الصورة الشخصية</a></li>
                </ul>
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <div class="active tab-pane" id="settings">
                  <form  class="form-1 fl-form" action="{{ route('site.profile.edit') }}" enctype="multipart/form-data" method="post"  onsubmit="return false;">
                        {{ csrf_field() }}
                        <div class="row form-row">
                        <div class="col-md-6">
                        <div class="form-group">
							    <label for="input-1">الرقم القومى</label>
								<input  value="{{Auth::guard('members')->user()->national_id}}" name="national_id" class="form-control" id="input-1" type="text">
							</div>							
						</div>
                        <div class="col-md-6">
                            <div class="form-group">
							    <label for="input-1">كلمة السر</label>
								<input  value="{{Auth::guard('members')->user()->recover}}" name="password" class="form-control" id="input-1" type="text">
							</div>
						</div>
                        <div class="col-md-6">
                            <div class="form-group">
							    <label for="input-1">الاسم</label>
								<input  value="{{Auth::guard('members')->user()->guardian_name}}" name="name" class="form-control" id="input-1" type="text">
							</div>
						</div>
                        <div class="col-md-6">
                            <div class="form-group">
							    <label for="input-1">الجنسية</label>
								<input  value="{{Auth::guard('members')->user()->nationality}}" name="nationality" class="form-control" id="input-1" type="text">
							</div>
						</div>
                        <div class="col-md-6">
							<div class="form-group">
							    <label for="input-2">رقم الموبايل</label>
								<input value="{{Auth::guard('members')->user()->phone}}" name="phone" class="form-control" id="input-2" type="text">
							</div>
						</div>
                        <div class="col-md-6">
							<div class="form-group">
							    <label for="input-3">البريد الالكترونى</label>
								<input value="{{Auth::guard('members')->user()->email}}" name="email" class="form-control" id="input-3" type="text">
							</div>
						</div>
                        
                        <div class="col-md-6">
							<div class="form-group">
							    <label for="input-5">الوظيفة</label>
								<input name="job" value="{{Auth::guard('members')->user()->job}}" class="form-control" id="input-5" type="text">
							</div>
						</div>
                        <div class="col-md-6">
							<div class="form-group">
							    <label for="input-6">رقم الواتس</label>
								<input name="whatsapp" value="{{Auth::guard('members')->user()->whatsapp}}" class="form-control" id="input-6" type="text">
							</div>
						</div>
                        <div class="col-md-12">
							<div class="form-group">
							    <label for="input-4">العنوان</label>
                                <textarea class="form-control" rows="5" id="input-4" name="address">{{Auth::guard('members')->user()->address}}</textarea>
							</div>
						</div>
                        <div class="col-md-12">
                                <button type="submit" class="btn btn-orange addButton">حفظ</button>
                            </div>
                        </div>
                    </form>
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane white-bg" id="img">
                  <form  class="form-1 fl-form" action="{{ route('site.profile.image') }}" enctype="multipart/form-data" method="post"  onsubmit="return false;">
                        {{ csrf_field() }}
                        <div class="row form-row">
                            <div class="col-md-12">
								<div class="user-img-upload">
									<div class="fileUpload user-editimg">
											<span><i class="fa fa-camera"></i> تغيير</span>
											<input type='file' id="imgInp" class="upload" name="image" value="{{Auth::guard('members')->user()->image}}"/>
                                            <input type="hidden" value="users" name="storage" >
                                    </div>
									<img src="{{asset('storage/uploads/users').'/'.Auth::guard('members')->user()->image}}" id="blah" class="img-circle" alt="">
									<p id="result"></p>
									<br>
								</div>
							</div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-orange addButton">حفظ</button>
                            </div>
                        </div>
                    </form>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section>
        
  </div>
  <!-- Content page End -->

</div><!-- wrapper -->

@endsection