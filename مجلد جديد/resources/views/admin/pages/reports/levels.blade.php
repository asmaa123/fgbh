@extends('admin.layouts.master')
@section('title')
تقارير المستويات
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper">
    <!--<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">Dashboard</span>
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>-->
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تقارير المستويات</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
				
                    <div class="box-body">
               <table id="tables" class="display dataTable no-footer dtr-inline" style="width:100%">
                        <thead>
                        <tr>
                            <th class="num">#</th>
                            <th>انوع الحلقه</th>
                            <th colspan="2" >المستوي الاول </th>
                            <th  colspan="2" >أالمستوي التاني </th>
                            <th  colspan="2" >االمستوي الثالث </th>
                            <th>الاجمالي</th>
                          
                        </tr>
                        
                        <tr class="tr-head">
                                <th>رقم التسلسل</th>
                                <th>انوع الحلقه</th>
                                 <th> بنات</th>
                                  <th> بنين</th>
                                 <th> بنات</th>
                                <th> بنين</th>
                               <th> بنات</th>
                               <th> بنين</th>
                               <th>الاجمالي</th>
                             
                        </tr>
                        </thead>
                        <tbody>
                           
                        @foreach($levels as $level)
                         <tr>
                            <td class="num">{{ $loop->iteration }}</td>
                           
                            <td>{{ isset($level->coursetype->type_name) ?     $level->coursetype->type_name     :''}}     {{$level->national_id}}</td>
                                <td>
                                 @if ($level->id==1 && $level->students->where('gender', 'ذكر'))
                                 
                                 {{$level->count()  }}
                                 
                                 
                                 
                                 @endif
                                    
                                    
                                </td>
                                
                              <td> 
                              
                              
                              
                                   @if ($level->id==1 && $level->students->where('gender', 'أنثى'))
                                 
                                 {{$level->count()  }}
                                 
                                 
                                 
                                 @endif
                              
                              
                              
                              
                              
                              
                              
                              </td>
                              
                                 <td>
                                     
                                     
                                   @if ($level->id==2 && $level->students->where('gender', 'ذكر'))
                                 
                                 {{$level->count()  }}
                                 
                                 
                                 
                                 @endif   
                                     
                                     
                                  </td>
                                 <td>
                                     
                                     
                                      
                                @if ($level->id==2 && $level->students->where('gender', 'أنثى'))
                                 
                                 {{$level->count()  }}
                                 
                                 
                                 
                                 @endif
                                     
                                     
                                     
                                 </td>
                                 <td>
                                     
                                     
                                     
                                  @if ($level->id==3 && $level->students->where('gender', 'ذكر'))
                                 
                                 {{$level->count()  }}
                                 
                                 
                                 
                                 @endif   
                                         
                                     
                                     
                                     
                                 </td>
                                 <td>
                                     
                               @if ($level->id==3 && $level->students->where('gender', 'أنثى'))
                                 
                                 {{$level->count()  }}
                                 
                                 
                                 
                                 @endif
                                       
                                     
                                     
                                     
                                 </td>
                                <td>{{$level->students->count()}}   </td>
                               </tr>
                       
                        @endforeach
                      
                        </tbody>
                    </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
  </div>
  <!-- Content page End -->
@endsection