<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;


    public function roles(){


        return $this->belongsToMany('App\Role');
    }


    public static function checkRole($user_id, $role){

        $roles = Role::select('*')
                    ->join('role_user', 'role_user.role_id', 'roles.id')
                    ->where('role_user.user_id', $user_id)
                    ->where('roles.name', $role)
                    ->get();
        if($roles){
            return true;
        }else{
            return false;
        }



    }

    public function getRolelistAttribute(){

     return $this->roles()->pluck('name','id')->toArray();


    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image','name','username','type','mobile','about', 'email', 'password','recover','country','facebook','twitter','google','instagram','phone','address','active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
