<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;
use App\Guardian;
use App\Course;
use App\Level;
use App\Material;
use App\Season;
use App\Center;
use App\Member;
use App\Town;
use App\CourseType;
use Config;
use DB;

class GuardiansController extends Controller
{
    public function getIndex() {
        $guardians = Guardian::where("active", 1)->get();
        return view('admin.pages.guardian.index', compact('guardians'));
    }

    public function getAdd() {
        $guardians = Guardian::where("active", 1)->get();
        return view('admin.pages.guardian.add', compact('guardians'));
    }

    public function insert(Request $request) {
        
       
       
        $guardian = new Guardian();
        $destination = storage_path('uploads/' . $request->storage);
        $image = $request->file('image');
        if ($image) {
            if (is_file($destination . "/{$image}")) {
                @unlink($destination . "/{$image}");
            }
            $imageName = $image->getClientOriginalName();
            $image->move($destination, $imageName);
            
            $guardian->image = $imageName;
        }
        $guardian->guardian_name = $request->name;
        $guardian->username = $request->username;
        $guardian->password = bcrypt($request->password);
        $guardian->recover = $request->password;
        $guardian->address = $request->address;
        $guardian->email = $request->email;
        $guardian->phone = $request->phone;
        $guardian->phone2 = $request->phone2;
        $guardian->whatsapp = $request->whatsapp;
        $guardian->national_id = $request->national_id;
        $guardian->nationality = $request->nationality;
        $guardian->job = $request->job;
        if($request->active == "on"){
            $guardian->active = 1;
        }elseif(empty($request->active)){
            $guardian->active = 0;
        }
        
        $member = Member::where("national_id",$request->national_id)->first();
        if($member){
            return redirect()->route('admin.guardians.edit' , ['id' => $member->id]); 
        }else{
            if ($guardian->save()){
                $member = new Member();
                $member->guardian_id = $guardian->id;
                $member->guardian_name = $request->name;
                $member->username = $request->username;
                $member->password = bcrypt($request->password);
                $member->recover = $request->password;
                $member->address = $request->address;
                $member->email = $request->email;
                $member->phone = $request->phone;
                $member->whatsapp = $request->whatsapp;
                $member->national_id = $request->national_id;
                $member->nationality = $request->nationality;
                $member->job = $request->job;
                $member->image = $guardian->image;
                $member->save();
                return redirect()->route('admin.guardians.edit' , ['id' => $guardian->id]);            
            }
        }
    }

    public function getEdit($id) {
        if (isset($id)) {
            $guardians = Guardian::where("active", 1)->get();
            $guardian = Guardian::find($id);
            $students = DB::table('students')
                ->join('centers','centers.id','=','students.center_id')
                ->join('seasons','seasons.id','=','students.season_id')
                ->join('counts','counts.student_id','=','students.id')
                ->select('students.*','centers.center_name','seasons.season_name','counts.amount')
                ->where("students.guardian_id",'=', $id)
                ->get();
            $seasons = Season::where("active", 1)->get();
            $centers = Center::where("active", 1)->get();
            //$levels = Level::where("active", 1)->get();
            $types = CourseType::where("active", 1)->get();
            $towns = Town::where("active", 1)->get();
            return view('admin.pages.guardian.edit', compact('guardian','guardians','students','seasons','centers','types','towns'));
        }        
    }

    public function postEdit(Request $request,$id) {
        $v = validator($request->all() ,[
            //'email' => 'required|string|email',
            'email' => 'required|email|max:255',

        ] ,[
            'email.required' => 'البريد الالكتروني مطلوب',
            'email.email' => 'البريد الالكتروني غير صحيح'
            
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }
        
        
        $guardian = Guardian::find($id);
        $guardian->guardian_name = $request->name;
        $guardian->username = $request->username;
        $guardian->password = bcrypt($request->password);
        $guardian->recover = $request->password;
        $guardian->address = $request->address;
        $guardian->email = $request->email;
        $guardian->phone = $request->phone;
        $guardian->phone2 = $request->phone2;
        $guardian->whatsapp = $request->whatsapp;
        $guardian->national_id = $request->national_id;
        $guardian->nationality = $request->nationality;
        $guardian->job = $request->job;
        if($request->active == "on"){
            $guardian->active = 1;
        }elseif(empty($request->active)){
            $guardian->active = 0;
        }

        $destination = storage_path('uploads/' . $request->storage);
        $image = $request->file('image');
        if ($image) {
            if (is_file($destination . "/{$image}")) {
                @unlink($destination . "/{$image}");
            }
            $imageName = $image->getClientOriginalName();
            $image->move($destination, $imageName);
            $guardian->image = $imageName;
        }
        if ($guardian->save()){
            $member = Member::where("guardian_id",$id)->first();
            $member->guardian_name = $request->name;
            $member->username = $request->username;
            $member->password = bcrypt($request->password);
            $member->recover = $request->password;
            $member->address = $request->address;
            $member->email = $request->email;
            $member->phone = $request->phone;
            $member->whatsapp = $request->whatsapp;
            $member->national_id = $request->national_id;
            $member->nationality = $request->nationality;
            $member->job = $request->job;
            $member->image = $guardian->image;
            $member->save();
            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function delete($id) {
        if (isset($id)) {
            $guardian = Guardian::find($id);
            $guardian->delete();

            return redirect()->back();
        }
    }
    
    public function getcentersdata() {

		$town_id = Input::get('id');

        $centers = Center::where('town_id','=',$town_id)->get();
		
        return Response::json($centers);
    }
    

    public function getcoursesdata() {

        $type_id = Input::get('id');
    
  $courses =  DB::table('courses')
              
                ->join('course_types','course_types.id','=','courses.coursetype_id')

                ->select('courses.*','course_types.type_name')
             
                ->where('center_id', '=',$type_id)
                ->get(); 

        return Response::json($courses);
        
        
    }



    public function getcoursesdataa() {

        $type_id = Input::get('id');

        $courses =  DB::table('courses')

            ->join('course_types','course_types.id','=','courses.coursetype_id')

            ->select('courses.*','course_name')

            ->where('coursetype_id', '=',$type_id)
            ->get();

        return Response::json($courses);


    }




    public function getcoursesdata1(){
        
         $level_id = Input::get('id');
         
          $courses =  DB::table('levels')
               
                ->join('course_types','course_types.id','=','levels.coursetype_id')
                 
                ->select('levels.*','course_types.type_name')
             
                ->where('coursetype_id', $level_id)
                ->get(); 

        return Response::json($courses);
        
    }


   

    public function getlevelsdata() {
        
        
    
        $type_id = Input::get('id');

        $levels = Level::where('coursetype_id','=',$type_id)->get();
		
        return Response::json($levels);
        

	/*	$course_id = Input::get('id');

        $levels = Level::where('course_id','=',$course_id)->get();
		
        return Response::json($levels);*/
        
    }

 public function getmaterialsdata() {

		$level_id = Input::get('id');

		$materials = DB::table('materials')
				->join('levels','materials.level_id','=','levels.id')

				->select('materials.*','levels.level_name','materials.material_name')
				->where('materials.level_id','=',$level_id)
				->get();

        return Response::json($materials);
    }
    

}
