<?php


namespace App\Helpers;


use App\Permission;
use App\Role;

class PermissonsHelper
{
    static function attachStudentPermissions() {

        $role = Role::findOrFail(1);
        $permissions = array(
            array('id' => '1','name' => 'عرض طالب'),
            array('id' => '3','name' => 'ملفات الطلاب '),
            array('id' => '4','name' => 'تعديل بيانات الطلاب'),
        );
        foreach ($permissions as $permission) {
            $permObject = Permission::findOrFail($permission['id']);
            $hasPermission = $role->hasPermission($permObject->name);
            if (!$hasPermission) {
                $role->attachPermission($permObject);
            }
        }
    }
}