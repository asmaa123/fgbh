<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apartment_payment extends Model 
{

    protected $table = 'apartment_payment';
    public $timestamps = true;
    protected $fillable = array('payment_id', 'partment_id', 'status');

}