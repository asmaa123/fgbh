@extends('admin.layouts.master')
@section('title')
تقارير حسابات الطلاب
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تقارير حسابات الطلاب</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
                    <div class="box-body">
                        <table id="tables" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th class="num">#</th>
                                <th>الطالب</th>
                                <th>الديون</th>
                                <th> الخصم</th>
                                <th>التاريخ</th>
                                <th>ملاحظات</th>
                            </tr>
                            <tr class="tr-head">
                                <th>الترتيب</th>
                                <th>الطالب</th>
                                <th>الديون</th>
                                <th> الخصم</th>
                                <th>التاريخ</th>
                                <th>ملاحظات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <td class="num"></td>
                                    <td>{{$student->student_name}}     {{$student->national_id}}</td>
                                    @if($student->amount > 0)
                                    <td>الرصيد : {{$student->amount}}</td>
                                    @elseif($student->amount < 0)
                                    <td>الديون : {{$student->amount * -1}}</td>
                                    @elseif($student->amount == 0)
                                    <td>الديون : 0</td>
                                    @endif
                                    <td>{{$student->discount}}</td>
                                    <td>{{$student->date}}</td>
                                    <td>{{$student->notes}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>



                </div>
            </div>
        </div>
    </section>


</div>
  <!-- Content page End -->
@endsection

