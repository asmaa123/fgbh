<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Apartment;
use Intervention\Image\Facades\Image;

class ApartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
       $apartments=Apartment::get();

        return view('admin.pages.apartment.index',compact('apartments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.pages.apartment.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $v = validator($request->all() ,[

            'number' => 'required',
            'floor' => 'required',
            'phone' => 'required',
            'owner' => 'required',


        ] ,[

            'number.required' => 'من فضلك أدخل رقم الشقه',
            'floor.required' => 'من فضلك أدخل رقم الدور ',
            'phone.required' => 'من فضلك أدخل الهاتف',
            'owner.required' => 'من فضلك أدخل اسم صاحب الشقه',

        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

     $apartment=new Apartment();

    $apartment->number =$request->number;

    $apartment->floor =$request->floor;

    $apartment->phone =$request->phone;

    $apartment->owner =$request->owner;

        if ($apartment->save()){

            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];


        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }

        return view('admin.pages.apartment.add');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        if(isset($id)) {

            $partment = Apartment::find($id);

            return view('admin.pages.apartment.edit', compact('partment'));


               }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request, $id)
  {

  if(isset($id)) {

    $apartment = Apartment::find($id);

    $apartment->number = $request->number;

    $apartment->floor = $request->floor;

    $apartment->phone = $request->phone;

    $apartment->owner = $request->owner;

    if ($apartment->save()) {

        return ['status' => 'succes', 'data' => 'تم تحديث البيانات بنجاح'];


    } else {
        return ['status' => false, 'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
    }


            }
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (isset($id)) {

            $apartment = Apartment::find($id);

            $apartment->delete();

            return redirect()->back();


        }
    }
}
