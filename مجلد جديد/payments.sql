/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : project

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-12-24 20:29:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `payments`
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `month` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apartment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of payments
-- ----------------------------
INSERT INTO `payments` VALUES ('1', '20', 'ط\r\n><؛', 'قبث', '12', 'قثسفل', 'بسيلا', 'ليبفلي', null, null, null);
INSERT INTO `payments` VALUES ('3', '1111111111', 'دفع شهري', '0100', '11111111', 'sddf', '2031/2032', '12', null, '2019-12-24 08:53:14', '2019-12-24 10:30:57');
INSERT INTO `payments` VALUES ('4', '1111111111', 'دفع شهري', null, '11111111', null, '2020/2021', '01', null, '2019-12-24 08:53:15', '2019-12-24 08:53:15');
INSERT INTO `payments` VALUES ('5', '1111111111', 'دفع شهري', null, '1111111177777', null, '2019/2020', '02', null, '2019-12-24 09:57:05', '2019-12-24 09:57:05');
INSERT INTO `payments` VALUES ('6', '1111111111', 'دفع شهري', null, '1111111177777', null, '2019/2020', '02', null, '2019-12-24 09:57:06', '2019-12-24 09:57:06');
INSERT INTO `payments` VALUES ('7', '1111111111', 'دفع طارءق', '0100', '11111111', 'sddf', '2034/2035', '11', null, '2019-12-24 10:31:24', '2019-12-24 10:31:24');
INSERT INTO `payments` VALUES ('8', '1111111111', 'دفع طارءق', '0100', '11111111', 'sddf', '2034/2035', '11', null, '2019-12-24 10:31:25', '2019-12-24 10:31:25');
INSERT INTO `payments` VALUES ('9', '1111111111', 'دفع طارءق', '0100', '11111111', 'sddf', '2034/2035', '11', null, '2019-12-24 10:31:32', '2019-12-24 10:31:32');
INSERT INTO `payments` VALUES ('10', '1111111111', 'دفع طارءق', '0100', '11111111', 'sddf', '2034/2035', '11', null, '2019-12-24 10:31:33', '2019-12-24 10:31:33');
INSERT INTO `payments` VALUES ('11', '1111111111', 'دفع طارءق', '0100', '11111111', 'sddf', '2034/2035', '11', null, '2019-12-24 10:32:26', '2019-12-24 10:32:26');
INSERT INTO `payments` VALUES ('12', '1111111111', 'دفع شهري', '0100', '11111111', 'sddf', '2032/2033', '12', null, '2019-12-24 10:32:46', '2019-12-24 10:32:46');
INSERT INTO `payments` VALUES ('13', '1111111111', 'دفع شهري', '0100', '11111111', 'sddf', '2032/2033', '12', null, '2019-12-24 10:32:47', '2019-12-24 10:32:47');
INSERT INTO `payments` VALUES ('14', 'sadsad', 'دفع شهري', 'asdasda', 'dsad', 'asdasdsa', '2019/2020', '09', '0', '2019-12-24 17:44:00', '2019-12-24 17:44:00');
INSERT INTO `payments` VALUES ('15', 'سشيشس', 'دفع شهري', 'يسشي', 'شسيسش', 'يشسيسشي', '2020/2021', '10', 'kilsds', '2019-12-24 18:20:02', '2019-12-24 18:20:02');
apartmentsapartmentsapartment_paymentapartments