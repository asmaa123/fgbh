<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1120224634748024', //Facebook API
        'client_secret' => '7303691d807eaff92bbb1657b96b1e', //Facebook Secret
        'redirect' => 'http://laravel.localhost/login/facebook/callback',
    ],

    'twitter' => [
        'client_id' => '8JS1GvRflY5g9N3kZc0heYTqL',
        'client_secret' => 'eYujptPLBAAzdHIz8hiqGgz4MkJTcmL1JAGEuEZsGU1MykjK',
        'redirect' => 'http://laravel.localhost/login/twitter/callback',
    ],
    'gmail' => [
        'client_id' =>'613342105618-7k47hvhl618v5ms9gjo2oc2hpc9kvtfe.apps.googleusercontent.com',
        'client_secret' => 'Vp_wScWE0oPDcN2lJmKUswzp',
        'redirect' => 'http://laravel.localhost/login/gmail/callback',
    ],


];
