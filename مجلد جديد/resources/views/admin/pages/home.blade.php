



@extends('admin.layouts.master')
@section('title')
حفاظ
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper pd">
    <section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>لوحة التحكم</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li class="active">لوحة التحكم</li>
      </ol>
    </section>

  </div>
@endsection
  <!-- Content page End -->

@section('footer')

@endsection