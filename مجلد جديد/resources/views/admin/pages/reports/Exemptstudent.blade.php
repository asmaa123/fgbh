@extends('admin.layouts.master')
@section('title')
تقارير بالطلاب المعفين من الرسوم
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold"> بالطلاب المعفين من الرسوم</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
                    <div class="box-body">
                      
                        <table id="tables" class="display dataTable no-footer dtr-inline" style="width:100%">
                            <thead>
                            <tr>
                                <th class="num">#</th>
                               <th>الطالب</th>
                                <th>النوع</th>
                                <th>المركز</th>
                                <th>الهاتف</th>
                                <th>نسبه الاعفاء</th>
                            <th>سبب الاعفاء</th>
                            </tr>

                        <tr class="tr-head">
                                <th>الترتيب</th>
                                 <th>الطالب</th>
                                <th>النوع</th>
                                <th>المركز</th>
                                <th>الهاتف</th>
                                <th>نسبه الاعفاء</th>
                            <th>سبب الاعفاء</th>
                                 </tr>

                            </thead>
                            <tbody>
                                
                            
                                
                            @foreach($students as $student)
                                <tr>
                                    <td class="num">{{ $loop->iteration }}</td>
                                    <td>{{$student->student_name}}    </td>
                                    <td>{{$student->gender}}</td>
                             
                                    <td>{{$student->center->center_name}}</td>
                                    <td>{{$student->phone}}</td>
                                    <td>{{isset($student->exemption->ratio) ? $student->exemption->ratio    : ''}}</td>
                                    <td>{{isset($student->exemption->reason) ? $student->exemption->reason    : ''}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

</div>

                </div>
            </div>
        </div>
    </section>


</div>
  <!-- Content page End -->
@endsection

