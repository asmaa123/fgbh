@extends('admin.layouts.master')
@section('title')
تقارير حضور الطلاب
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold"> تقارير حضور الطلاب</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
                    <div class="box-body">
                        <table id="tables" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th class="num">#</th>
                                <th>الطالب</th>
                                <th>الحلقة</th>
                                <th>المركز</th>
                                <th>الحالة</th>
                                <th>التاريخ</th>
                            </tr>
                            <tr class="tr-head">
                                <th>الترتيب</th>
                                <th>الطالب</th>
                                <th>الحلقة</th>
                                <th>المركز</th>
                                <th>الحالة</th>
                                <th>التاريخ</th>
                            </tr>
                            </thead>
                            <tbody>
                              
                                
                            @foreach($students as $student)
                                <tr>
                                    <td class="num"></td>
                                    <td>{{ isset($student->student->student_name) ?   $student->student->student_name  : ''}}   {{$student->national_id}}</td>
                                    <td>{{  isset($student->course_name) ?  $student->course_name : ''}}</td>
                                    <td>{{   isset($student->center_name) ?  $student->center_name  : ''}}</td>
                                    @if($student->status == 1)
                                    <td>حاضر</td>
                                    @else
                                    <td>غائب</td>
                                    @endif
                                    <td>{{$student->date}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>



                </div>
            </div>
        </div>
    </section>


</div>
  <!-- Content page End -->
@endsection

