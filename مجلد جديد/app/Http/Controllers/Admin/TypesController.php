<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CourseType;
use App\Course;
use App\Center;
use App\Type;
use App\Level;
use Config;
use Response;
use DB;

class TypesController extends Controller
{
    public function getIndex() {
        $types = CourseType::where("active", 1)->get();
        return view('admin.pages.type.index', compact('types'));
    }
    
    public function getTypeWithCoursesInAjax($type_id) {
        $types = Course::where('coursetype_id',$type_id)->pluck("course_name","id");
        return Response::json($types);
    }
   
    public function getcenter($town_id) {
        $types = Center::where('town_id',$town_id)->pluck("center_name","id");
        return Response::json($types);
    }


   public function getTypeWithlevelInAjax($type_id) {
       
        $types = Level::where('coursetype_id',$type_id)->pluck("level_name","id");
        return Response::json($types);
    }


 public function getTypeWithCenterInAjax($center_id) {




         $types =  DB::table('courses')
      
                ->join('course_types','course_types.id','=','courses.coursetype_id')
                ->select('courses.*','course_types.type_name')
             
                ->where('center_id',$center_id)
                ->get(); 

      
            //with(['center'])->get();
        return Response::json($types);
    }

    public function getAdd() {
        $types = CourseType::where("active", 1)->get();
        return view('admin.pages.type.add', compact('types'));
    }

    public function insert(Request $request) {
        $v = validator($request->all() ,[
            'name' => 'required',
        ] ,[
            'name.required' => 'من فضلك أدخل اسم الفصل الدراسي',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $type = new CourseType();

        $type->type_name = $request->name;
      
        if($request->active == "on"){
            $type->active = 1;
        }elseif(empty($request->active)){
            $type->active = 0;
        }
        
        if ($type->save()){
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function getEdit($id) {
        if (isset($id)) {
            $types = CourseType::where("active", 1)->get();
            $type = CourseType::find($id);
            return view('admin.pages.type.edit', compact('type','types'));
        }        
    }

    public function postEdit(Request $request,$id) {

        $type = CourseType::find($id);
        $type->type_name = $request->name;
        if($request->active == "on"){
            $type->active = 1;
        }elseif(empty($request->active)){
            $type->active = 0;
        }

        if ($type->save()){
            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function delete($id) {
        if (isset($id)) {
            $type = CourseType::find($id);
            $type->delete();

            return redirect()->back();
        }
    }

}
